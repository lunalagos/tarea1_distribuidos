import java.io.IOException;
import java.net.*;
import java.util.concurrent.ThreadLocalRandom;


class ThreadServer extends Thread {
   private Thread t;
   private String threadServerName;
   private String typeserver;
   private String INET_ADDR; 
   private int PORT;
   public String msg;
   int medicion = 1;
   int i = 1;

   ThreadServer( String tiposervidor) {
    typeserver = tiposervidor;

    switch (typeserver){
    case "1":
      threadServerName = "Servidor de Humedad";
      //System.out.println("Creando  " +  threadServerName );
          INET_ADDR = "224.0.0.3";
          PORT = 8881;
          msg = "" + medicion;
          //delta = 20;
      break;
    case "2":
      threadServerName = "Servidor de Presion";
      //System.out.println("Creando  " +  threadServerName );
          INET_ADDR = "224.0.0.3";
          PORT = 8882;
          msg = "" + medicion;
          //delta = 100;
      break;
    case "3":
      threadServerName = "Servidor de Temperatura";
      //System.out.println("Creando  " +  threadServerName );
          INET_ADDR = "224.0.0.3";
          PORT = 8883;
          msg = "" + medicion;
          //delta = 1;
      break;
    case "4":
      threadServerName = "Servidor de Concentracion de CO2";
      //System.out.println("Creando  " +  threadServerName );
          INET_ADDR = "224.0.0.3";
          PORT = 8884;
          msg = "" + medicion;
          //delta = 150;
      break;
    default:
      threadServerName = "Servidor de Humedad";
      System.out.println("Creando  " +  threadServerName );
          INET_ADDR = "224.0.0.3";
          PORT = 8881;
          msg = "" + medicion;
          //delta = 20;
      break;
    }
   }

   public void run() {
      InetAddress addr = null;
    try {
      addr = InetAddress.getByName(INET_ADDR);
    } catch (UnknownHostException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
     
        // Open a new DatagramSocket, which will be used to send the data.
        try (DatagramSocket serverSocket = new DatagramSocket()) {
            while ( true ) {
       switch (typeserver){
          case "1":
            medicion =ThreadLocalRandom.current().nextInt(40, 90 + 1); // %
            break;
          case "2":
            medicion =ThreadLocalRandom.current().nextInt(60, 120 + 1);  //bares
            break;
          case "3":
            medicion =ThreadLocalRandom.current().nextInt(55, 80 + 1);  // C
            break;
          case "4":
            medicion =ThreadLocalRandom.current().nextInt(400, 403 + 1);  //ppm
            break;
          default:
            medicion =ThreadLocalRandom.current().nextInt(40, 90 + 1); // NN
            break;
          }
                msg = typeserver + ":" + i + ":" + medicion;
                // Create a packet that will contain the data
                // (in the form of bytes) and send it.
                DatagramPacket msgPacket = new DatagramPacket(msg.getBytes(),
                        msg.getBytes().length, addr, PORT);
                serverSocket.send(msgPacket);


                //Almacenar

     
                System.out.println(threadServerName + " a enviado la medicion Nro " + i + ": " + medicion);
                try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
              i = i + 1;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
   }

   public void start () {
      //System.out.println("Corriendo " +  threadServerName );
      if (t == null) {
         t = new Thread (this, typeserver);
         t.start ();
      }
   }

}


public class Server {

   public static void main(String args[]) {

      byte[] data;
      DatagramPacket packet;
      int packetSize = 1024;

      DatagramSocket socket = null;

      try {
        socket = new DatagramSocket(8891);
      } catch (SocketException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      int enviando = 0;

      InetAddress clientAddr;
      int clientPort;

      String mensaje;


      ThreadServer T1 = new ThreadServer("1");
      T1.start();

      ThreadServer T2 = new ThreadServer("2");
      T2.start();

      ThreadServer T3 = new ThreadServer("3");
      T3.start();

      ThreadServer T4 = new ThreadServer("4");
      T4.start();



      for (;;){

        data = new byte[packetSize];
        packet = new DatagramPacket(data, packetSize);

        try {
            // esperar indefinidamente la llegada de un paquete
          socket.receive(packet);

        } 
        catch (IOException ie) {
          System.out.println("[Servidor Central]: No se pudo recibir:" + ie.getMessage());
          continue;
        }

        try {
            // Obtener datos del cliente para enviar respuesta
          clientAddr = packet.getAddress();
          clientPort = packet.getPort();
          mensaje = new String(data, 0, packet.getLength(), "UTF-8"); // nombre distrito

          String[] data_mensaje = mensaje.split(":");
          String tipo = data_mensaje[0];
          String puerto_recu = data_mensaje[1];

          if (tipo.equals("1")) {
            enviando=1;
            ///////////////////////ENVIAR HISTORIAL HUMEDAD
          }
          if (tipo.equals("2")) {
            enviando=1;
            ///////////////////////ENVIAR HISTORIAL PRESION
          }
          if (tipo.equals("3")) {
            enviando=1;
            ///////////////////////ENVIAR HISTORIAL TEMPERATURA
          }
          if (tipo.equals("4")) {
            enviando=1;
            ///////////////////////ENVIAR HISTORIAL CONCENTRACIÓN
          }
        }
        catch (IOException ie) {
          System.out.println("[Servidor Central]: No se pudo recibir:" + ie.getMessage());
          continue;
        }

      }
      
      //ThreadServer T2 = new ThreadServer( "Servidor Temperatura","224.0.0.4", 8884);
      //T2.start();
   }   
}