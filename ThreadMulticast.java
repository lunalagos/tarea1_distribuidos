import java.net.*;
import java.io.*;
import java.util.*;

//hilo para la comunicacion Multicast
public class ThreadMulticast extends Thread {

    MulticastSocket receptor; // receptor de mensajes multicast

    //private boolean continue = true;
    InetAddress addr_mult; // Hacia donde enviar y recibir
    int p_multicast; // puertos de servidor distr y multicast
    String msgmulti; // lo que se obtiene del servidor
    byte[] datamult; // buffer para mensaje multicast

    public ThreadMulticast(int puerto, InetAddress address ){
        addr_mult = address;
        p_multicast = puerto;
    }

    public void run() {
	    for (; ; ) {
            try {
                receptor = new MulticastSocket(p_multicast);
                receptor.joinGroup(addr_mult);
                datamult = new byte[1024];
                DatagramPacket paquete = new DatagramPacket(datamult,datamult.length);
                receptor.receive(paquete);
                msgmulti = new String (paquete.getData());

                String[] data = msgmulti.split(":");
                String tipo = data[0];
                String id = data[1];
                String medicion = data[2];
                String threadServerName;

                switch (tipo){
                    case "1":
                        threadServerName = "Servidor de Humedad";
                        System.out.println(threadServerName + " a enviado medicion Nro " + id +": " + medicion + "%");
                        break;
                    case "2":
                        threadServerName = "Servidor de Presion";
                        System.out.println(threadServerName + " a enviado medicion Nro " + id +": " + medicion + " bares");
                        break;
                    case "3":
                        threadServerName = "Servidor de Temperatura";
                        System.out.println(threadServerName + " a enviado medicion Nro " + id +": " + medicion + " C");
                        break;
                    case "4":
                        threadServerName = "Servidor de Concentracion de CO2";
                        System.out.println(threadServerName + " a enviado medicion Nro " + id +": " + medicion + " ppm");
                        break;
                    default:
                        threadServerName = "Servidor de Humedad";
                        System.out.println(threadServerName + " a enviado medicion Nro " + id +": " + medicion + "%");
                        break;
                }
            } catch (IOException iee) {
                System.out.println("[Cliente]: Error = " + iee.getMessage());
                System.exit(0);
            }
	    }
    } 
} 